import requests
from os import path

class SlackNotifier:
    WEBHOOK_BASE = 'https://hooks.slack.com/services/'
    TOKENS = {'isiltakan': 'token_goes_here'}
    # number of channels can be increased
    def __init__(self, channel = 'isiltakan'):
        self.channel = channel

    def notify(self, msg):
        if self.channel not in self.TOKENS:
            self.channel = 'isiltakan'
        payload = {
            'text': '{}'.format(msg)
        }
        req = requests.post(self.WEBHOOK_BASE + self.TOKENS[self.channel],
                            json=payload,
                            headers={'Content-Type': 'application/json'})
        return req.text
    class PrintWrapper:
        def __init__(self, slack_notifier):
            self.slack_notifier = slack_notifier

        def __call__(self, *args, **kwargs):
            file_name = path.basename(__file__)
            message = ' '.join(map(str, args))
            message = '*' + file_name + ':*\n' + message
            self.slack_notifier.notify(message)
            print(*args, **kwargs)

    def nprint(self):
        wrapper = self.PrintWrapper(self)
        return wrapper

        
        