from setuptools import setup

setup(
    name='slack-notifier',
    version='0.1',
    description='Slack notification module',
    author='isitak',
    py_modules=['slack_notifier'],
    install_requires=['requests'],
)